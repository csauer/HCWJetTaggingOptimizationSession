
<div align="center" style="font-size: 100px">
  <h1 style="font-size:28px">
    <br>Organization Hadronic Callibration Workshop 2020<br/> 
    <br> –<br/> 
    <br> Jet Tagging Optimization<br/>
  </h1>
</div>

# Author(s)

- Christof Sauer <christof.sauer@cern.ch> Ruprecht-Karls Universität Heidelberg
- Josu Centero <Josu.Cantero.Garcia@cern.ch> Oklahoma State University

# General

Indico page of event with list of contributions: https://indico.cern.ch/event/939174/timetable/?view=standard

# Compile TEX document

If the full version of TexLive is isntalled on your system, all you have to do is to source the bash script in the bin directory

'''bash
source bin/pdftex
pdftex jetTaggingSession.tex
''

# Previous workshops

* 2019 [https://indico.cern.ch/event/798446/contributions/3595825/attachments/1928576/3193518/HCW2019_Jet_Tagging_Definitions.pdf]

# (Possible) topics

Following a compilation of possible topics (inspired by previous HCWs) with some suggestions regarding its respective content

## Boosted object tagging with TAR jets

* Josu
* Talk: [https://indico.cern.ch/event/910074/contributions/3828339/attachments/2021374/3379931/JSS_16April.pdf]

## Four-prong tagger

* Blake

## Truth Labeling Studies

* Chris, Josu
* Last HCW (2019), Josu proposed to include information provided by JSS variables in the truth labelling definition [cf. https://indico.cern.ch/event/798446/contributions/3595825/attachments/1928576/3193518/HCW2019_Jet_Tagging_Definitions.pdf, p. 9]
* **How does the containment requirement affect the modeling uncertainties?**
* Show different truth labels contained[R21.0, R20.7,split23varied], inclusive)
* New truth labelling from Josu availabe using split23 (cut on splitting scale pT dependent) [https://indico.cern.ch/event/931498/contributions/3921872/attachments/2064188/3463746/JSS_25June.pdf]
* May show effect of different truth labels (contained[R21.0, R20.7,split23varied], inclusive)
* "Design top truth labeling strategy independent on the trimming procedure"
* MC dependence due to ungroomed m and split23?
* **Why no comparison with Sherpa?**

## Constituent DNN-based tagger [proposal]

* **Should a DNN tagger be studied that takes as input the consitutents of the jet instead of higher-level JSS variables?**

## Multi-Class Boosted Object Tagger

* Garabed Halladjian

## Multi-Class Boosted Object Tagger

* Elena Freundlich et al

## Simple (smoothed) Cut-based Top Tagger

* Christof Sauer

## Simple (smoothed) Cut-based W/Z Tagger

* Xiang Chen

## JSSV-based DNN-Tagger

* Christof Sauer

## Quark-Gluon tagging

* Ke Li and Wanyun*

## MDT [W/Z-tagging]

* Davide Melini, Tobias itschen, Gang Zhang
* Currently, only results for W tagging
* Problem: Calibration of V+jets difficult due to S + B ~ B (difficult to get SFs)
* Use MDT to extract SFs in background-dominated region
* Hope to use V + Jets for data-driven calibration up to ~ 1TeV
* **Will those MDT make it to the next recommendation?**
* **Why only used for W tagging and not top tagging?**
* **How does MC/Data look like? SFs?**

## The Lund Jet Plane [W-tagging]

* Alex Sopio
* Distinct topologies for Lund jet planes; perfectly suited for ML
* Currently, only results for tracks no UFOs
* Currently, only results for W tagging
* Utilizing RNNs (LSTMs and GRUs)
* Significant **improvement** at high p_T (10x more bkg rejection for p_T > 1.5 TeV)
* **Deterioration** at low p_T (30 % smaller for p_T < 1.5 TeV)
* **On slide 7, how are the uncertainty bands computed?**
* **Top tagging?**
* **Results for UFO jets?**
* **Do we observe mass sculpting? Does the tagged mass spectrum of the looks lignal-like? Are the variables correlated to the jet mass at all?**
